const fs = require('fs');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const Tour = require('../../models/tourModel');

dotenv.config({ path: `config.env` });
const database = process.env.DATABASE;

mongoose
  .connect(database, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
  })
  .then(() => {
    console.log('db connection successful');
  });

const tours = fs.readFileSync(`${__dirname}/tours-simple.json`, 'utf-8');

const removeData = async () => {
  try {
    await Tour.deleteMany();
    console.log('all data successfuly deleted!');
  } catch (err) {
    console.log(err);
  }

  process.exit();
};

const importData = async () => {
  try {
    await Tour.create(JSON.parse(tours));
    console.log('all data successfuly created!');
  } catch (err) {
    console.log(err);
  }

  process.exit();
};

if (process.argv[2] === '--import') {
  setTimeout(importData, 1000);
} else if (process.argv[2] === '--delete') {
  setTimeout(removeData, 1000);
}
