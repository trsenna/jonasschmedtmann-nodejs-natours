const dotenv = require('dotenv');
const mongoose = require('mongoose');

dotenv.config({ path: 'config.env' });
const app = require('./app');

const appPort = process.env.APP_PORT;
const database = process.env.DATABASE;

mongoose
  .connect(database, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
  })
  .then(() => {
    console.log('db connection successful');
  });

app.listen(appPort, () => {
  console.log(`App running on port ${appPort}...`);
});
